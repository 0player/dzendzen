﻿using DzenDzen;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace DzenDemoOne
{
    public class BallPit : GameComponent
    {
        private int numRows;

        public const int ROWSIZE = 6;

        BallType[,] ballPit;

        static BallType[] allTypes = { BallType.Herba, BallType.Ignis, BallType.Aqua, BallType.Terra, BallType.Caeli, BallType.Glacies };

        int rowToFill = 0;
        SortedSet<BallType> freeTypes = new SortedSet<BallType>(allTypes);
        int currentBallColumn = 2;
        BallType selectedBall = BallType.Herba;

        BallType[] combination = null;
        Random rng;

        struct Guess
        {
            public byte light;
            public byte shade;
        }

        Guess[] guesses;

        private BallPitView view;
        private HintView hintView;

        public BallPit(Game game, BallPitView view, HintView hintView, int rows )
            : base(game)
        {
            numRows = rows;

            ballPit = new BallType[numRows, ROWSIZE];
            guesses = new Guess[numRows];

            this.view = view;
            this.hintView = hintView;

            UpdateOrder = 2;
        }

        private int CurrentBallRow { get => rowToFill + 1; }

        public override void Initialize()
        {
            using (RNGCryptoServiceProvider seed = new RNGCryptoServiceProvider())
            {
                var rnb = new byte[4];
                seed.GetBytes(rnb);
                rng = new Random(BitConverter.ToInt32(rnb, 0));
            }

            view.SetBall(CurrentBallRow, currentBallColumn, selectedBall);

            base.Initialize();
        }

        public void BallNext()
        {
            NextBallType();
            view.ScrollToLeft(CurrentBallRow, currentBallColumn, selectedBall);
        }

        public void BallPrev()
        {
            PrevBallType();
            view.ScrollToRight(CurrentBallRow, currentBallColumn, selectedBall);
        }

        private void NextBallType()
        {
            if (selectedBall >= freeTypes.Max)
            {
                selectedBall = freeTypes.Min;
            }
            else
            {
                selectedBall = freeTypes.GetViewBetween((BallType)((int)(selectedBall) + 1), freeTypes.Max).Min;
            }
        }

        private void PrevBallType()
        {
            if (selectedBall <= freeTypes.Min)
            {
                selectedBall = freeTypes.Max;
            }
            else
            {
                selectedBall = freeTypes.GetViewBetween(freeTypes.Min, (BallType)((int)selectedBall - 1)).Max;
            }
        }

        public void BallLeft()
        {
            var newBallColumn = Math.Max(0, currentBallColumn - 1);
            if (newBallColumn != currentBallColumn)
            {
                view.SlideToLeft(CurrentBallRow, currentBallColumn);
                currentBallColumn = newBallColumn;
            }
        }


        public void BallRight()
        {
            var newBallColumn = Math.Min(ROWSIZE - 1, currentBallColumn + 1);
            if (newBallColumn != currentBallColumn)
            {
                view.SlideToRight(CurrentBallRow, currentBallColumn);
                currentBallColumn = newBallColumn;
            }
        }

        public void BallDrop()
        {
            if (ballPit[rowToFill, currentBallColumn] == BallType.NIHIL)
            {
                ballPit[rowToFill, currentBallColumn] = selectedBall;
                freeTypes.Remove(selectedBall);
                if (freeTypes.Count == 0)
                {
                    view.SlideDown(CurrentBallRow, currentBallColumn);
                    NextRow();
                }
                else
                {
                    NextBallType();
                    view.SlideDown(CurrentBallRow, currentBallColumn, selectedBall);
                }
            }
            else
            {
                BallType oldBall = ballPit[rowToFill, currentBallColumn];
                freeTypes.Remove(selectedBall);
                freeTypes.Add(oldBall);
                ballPit[rowToFill, currentBallColumn] = selectedBall;
                selectedBall = oldBall;
                view.SwapWithLower(CurrentBallRow, currentBallColumn);
            }
        }

        public void BallYank()
        {
            if (ballPit[rowToFill, currentBallColumn] != BallType.NIHIL)
            {
                selectedBall = ballPit[rowToFill, currentBallColumn];
                freeTypes.Add(selectedBall);
                ballPit[rowToFill, currentBallColumn] = BallType.NIHIL;
                view.SlideUpInto(CurrentBallRow, currentBallColumn);
            }
        }

        public void Restart()
        {
            view.ScreenFadeLose(rowToFill);
            Clear();
            hintView.Clear();
        }

        protected void NextRow() {
            if (combination == null)
            {
                combination = (BallType[])allTypes.Clone();
                // shuffle the shit
                for (int i = 0; i < ROWSIZE; ++i)
                {
                    int idxwith = rng.Next(i, ROWSIZE);
                    var t = combination[i];
                    combination[i] = combination[idxwith];
                    combination[idxwith] = t;
                }
            }
            for (int i = 0; i < ROWSIZE; ++i)
            {
                // TODO: animation.
                if (ballPit[rowToFill, i] == combination[i])
                    ++guesses[rowToFill].light;
                else if (ballPit[rowToFill, i] == combination[i].Shade())
                {
                    ++guesses[rowToFill].shade;
                }
            }
            hintView.SetHints(rowToFill, guesses[rowToFill].light, guesses[rowToFill].shade);
            if (guesses[rowToFill].light == ROWSIZE
                || guesses[rowToFill].shade == ROWSIZE
                || rowToFill == numRows - 1 && FairAttemptCheck())
            {
                view.ScreenFadeWin(rowToFill + 1);
                Clear();
                hintView.Clear();
            }
            else
            {
                ++rowToFill;
                if (rowToFill >= numRows)
                {
                    view.ScreenFadeLose(rowToFill);
                    Clear();
                    hintView.Clear();
                }
            }
            currentBallColumn = 2;
            freeTypes = new SortedSet<BallType>(allTypes);
            NextBallType();
            view.SetBall(CurrentBallRow, currentBallColumn, selectedBall);
        }

        private bool FairAttemptCheck()
        {
            for (int row1 = 1; row1 < numRows; ++row1)
            {
                for (int row2 = 0; row2 < row1; ++row2)
                {
                    bool equal = true;
                    for (int col = 0; col < ROWSIZE; ++col)
                    {
                        equal &= ballPit[row1, col] == ballPit[row2, col];
                    }
                    // Repeating rows - no fair chance.
                    if (equal) return false;
                }
            }
            // Check if last fair attempt fits all the rows.
            int lastFairAttempt = numRows - 2;
            for (int row = 0; row < lastFairAttempt; ++row)
            {
                byte light = 0, shade = 0;
                for (int col = 0; col < ROWSIZE; ++col)
                {
                    if (ballPit[row, col] == ballPit[lastFairAttempt, col]) ++light;
                    else if (ballPit[row, col] == ballPit[lastFairAttempt, col].Shade()) ++shade;
                }
                if (light != guesses[row].light || shade != guesses[row].shade) return false;
            }
            return true;
        }

        private void Clear()
        {
            for (int row = 0; row < numRows; ++row)
            {
                for (int column = 0; column < ROWSIZE; ++column)
                {
                    ballPit[row, column] = BallType.NIHIL;
                }
                guesses[row] = new Guess();
            }
            rowToFill = 0;
            combination = null;
        }
    }
}