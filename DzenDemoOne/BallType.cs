﻿namespace DzenDzen
{
    public enum BallType
    {
        NIHIL,
        Herba,
        Ignis,
        Aqua,
        Terra,
        Caeli,
        Glacies,
    }

    public static class BallTypeExtensions
    {
        public static BallType Shade(this BallType ball)
        {
            return (BallType)(1 + ((int)ball - 1 + 3) % 6);
        }
    }
}