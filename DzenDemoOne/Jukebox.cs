﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace DzenDemoOne
{
    public class Jukebox : GameComponent
    {
        private Song song;

        private Dictionary<string, SoundEffect> sounds = new Dictionary<string, SoundEffect>();

        public Jukebox(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            StartSong();
            base.Initialize();
        }

        public void StartSong()
        {
            song = Game.Content.Load<Song>("music/Gi Mekemume");
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.1f;
        }

        public void PlaySound(string name)
        {
            if (!sounds.ContainsKey(name))
            {
                sounds[name] = Game.Content.Load<SoundEffect>("sound/" + name);
            }

            var instance = sounds[name].CreateInstance();
            instance.Play();
        }
    }
}
