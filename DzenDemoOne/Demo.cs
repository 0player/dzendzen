﻿using DzenDzen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace DzenDemoOne
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Demo : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int ROWS = 10;
        const int COMBINATIONS = 720;
        const int BALL_SIZE = 42;
        const int HINT_WIDTH = 21;
        const int WIDTH = 400;
        const int HEIGHT = 470;

        enum UserInteraction
        {
            Left,
            Right,
            PrevBall,
            NextBall,
            Drop,
            Yank
        }

        struct UIPair
        {
            public Keys firstKey;
            public Keys secondKey;
            public UserInteraction firstUI;
            public UserInteraction secondUI;

            public UIPair(Keys firstKey, Keys secondKey, UserInteraction firstUI, UserInteraction secondUI)
            {
                this.firstKey = firstKey;
                this.secondKey = secondKey;
                this.firstUI = firstUI;
                this.secondUI = secondUI;
            }
        }

        Dictionary<UserInteraction, TimeSpan> throttleInteractions = new Dictionary<UserInteraction, TimeSpan> { };
        Queue<UserInteraction> userInputs = new Queue<UserInteraction> { };

        static TimeSpan throttleInterval = new TimeSpan(0, 0, 0, 0, 200);

        static List<UIPair> key2User = new List<UIPair> {
            new UIPair(Keys.Q, Keys.E, UserInteraction.PrevBall, UserInteraction.NextBall ),
            new UIPair(Keys.A, Keys.D, UserInteraction.Left, UserInteraction.Right),
            new UIPair(Keys.W, Keys.S, UserInteraction.Yank, UserInteraction.Drop)
        };
        private UserInteraction lastInteraction;

        private readonly BallPit ballPit;
        private readonly BallPitView ballPitView;

        public Demo()
        {
            const int ROWSIZE = BallPit.ROWSIZE;
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = WIDTH;
            graphics.PreferredBackBufferHeight = HEIGHT;
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            Content.RootDirectory = "Content";
            var jukebox = new Jukebox(this);

            ballPitView = new BallPitView(this, jukebox, ROWS + 1, ROWSIZE, BALL_SIZE,
                new Rectangle(0, 0, ROWSIZE * BALL_SIZE, HEIGHT));
            var hintView = new HintView(this, ROWS, ROWSIZE, HINT_WIDTH, BALL_SIZE, new Rectangle(WIDTH - ROWSIZE * HINT_WIDTH, 0, ROWSIZE * HINT_WIDTH, HEIGHT));
            ballPit = new BallPit(this, ballPitView, hintView, ROWS);

            Components.Add(ballPitView);
            Components.Add(hintView);
            Components.Add(ballPit);

            Components.Add(jukebox);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            var state = Keyboard.GetState();

            ballPitView.IsShade = state.IsKeyDown(Keys.Z);

            if (state.IsKeyDown(Keys.M))
                MediaPlayer.IsMuted = !MediaPlayer.IsMuted;

            if (state.IsKeyDown(Keys.R))
                ballPit.Restart();

            // bluh bluh order
            foreach (var pair in key2User)
            {
                if (state.IsKeyDown(pair.firstKey) && state.IsKeyDown(pair.secondKey))
                {
                    continue;
                }
                if (state.IsKeyDown(pair.firstKey))
                {
                    PushInteraction(pair.firstUI, gameTime.TotalGameTime);
                }
                else if (state.IsKeyDown(pair.secondKey))
                {
                    PushInteraction(pair.secondUI, gameTime.TotalGameTime);
                }
            }

            if (userInputs.Count != 0)
            {
                switch (lastInteraction = userInputs.Dequeue())
                {
                    case UserInteraction.Left:
                        ballPit.BallLeft();
                        break;
                    case UserInteraction.Right:
                        ballPit.BallRight();
                        break;
                    case UserInteraction.PrevBall:
                        ballPit.BallPrev();
                        break;
                    case UserInteraction.NextBall:
                        ballPit.BallNext();
                        break;
                    case UserInteraction.Drop:
                        ballPit.BallDrop();
                        break;
                    case UserInteraction.Yank:
                        ballPit.BallYank();
                        break;
                    default:
                        break;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }

        private bool PushInteraction(UserInteraction intr, TimeSpan timeat)
        {
            //
            if (!throttleInteractions.ContainsKey(intr) || (timeat - throttleInteractions[intr]) >= throttleInterval)
            {
                throttleInteractions[intr] = timeat;
                userInputs.Enqueue(intr);
                return true;
            }
            return false;
        }
    }
}
