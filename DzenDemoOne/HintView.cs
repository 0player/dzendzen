﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DzenDemoOne
{
    public class HintView : DrawableGameComponent
    {
        private Rectangle drawableArea;
        private Point hintArea;
        private int rows, cols;

        private Stopwatch stopwatch = new Stopwatch();

        private static readonly TimeSpan interval = TimeSpan.FromMinutes(1.0 / 188);
        private static readonly TimeSpan negshift = TimeSpan.FromMilliseconds(0);

        private SoundEffect lightMatch;
        private SoundEffect shadeMatch;

        private Texture2D lightHint;
        private Texture2D shadeHint;

        private SpriteBatch spriteBatch;

        byte[] lightHintsNo;
        byte[] shadeHintsNo;

        TimeSpan[][] hintBeats;

        Task chain;

        CancellationTokenSource cancel;

        public HintView(Game game, int numRows, int numCols, int hint_width, int hint_height, Rectangle drawableArea) : base(game)
        {
            rows = numRows;
            cols = numCols;
            this.drawableArea = drawableArea;
            this.hintArea = new Point(hint_width, hint_height);
            Debug.Assert(numCols == 6);
            hintBeats = new TimeSpan[numRows][];
            lightHintsNo = new byte[numRows];
            shadeHintsNo = new byte[numRows];
            cancel = new CancellationTokenSource();
        }

        public override void Initialize()
        {
            stopwatch.Start();
            base.Initialize();
        }

        public void SetHints(int row, byte light, byte shade)
        {
            lightHintsNo[row] = light;
            shadeHintsNo[row] = shade;
            var token = cancel.Token;
            if (chain != null)
            {
                chain = chain.ContinueWith((unused) => RunnerTask(row, light, shade, token), token);
            }
            else
            {
                chain = RunnerTask(row, light, shade, token);
            }
        }

        public void Clear()
        {
            cancel.Cancel();
            try
            {
                chain?.Wait();
            }
            catch
            {
                if (!chain.IsCanceled)
                    throw;
            }
            cancel.Dispose();
            cancel = new CancellationTokenSource();

            for (int row = 0; row < rows; ++row)
            {
                lightHintsNo[row] = 0;
                shadeHintsNo[row] = 0;
                lock (hintBeats) hintBeats[row] = null;
            }
        }

        public override void Draw(GameTime time)
        {
            var reading = Reading;

            spriteBatch.Begin();
            for (int row = 0; row < rows; ++row)
            {
                TimeSpan[] beats;
                lock (hintBeats) beats = hintBeats[row];
                if (beats == null)
                    continue;
                int commonNo = lightHintsNo[row] + shadeHintsNo[row];
                for (int col = 0; col < commonNo; ++col)
                {
                    var delay = (reading - beats[col]).Ticks / (float)interval.Ticks;
                    var rect = HintPosition(row, col);
                    var sprite = (col < lightHintsNo[row]) ? lightHint : shadeHint;
                    spriteBatch.Draw(sprite, rect, Color.White * Math.Max(0.0f, Math.Min(1.0f, delay)));
                }
            }
            spriteBatch.End();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);

            lightHint = Game.Content.Load<Texture2D>("hints/Light");
            shadeHint = Game.Content.Load<Texture2D>("hints/Shade");

            lightMatch = Game.Content.Load<SoundEffect>("sound/True match");
            shadeMatch = Game.Content.Load<SoundEffect>("sound/Shadow match");

            base.LoadContent();
        }

        private static IEnumerable<int> Pauses(byte light, byte shade)
        {
            if (light + shade < 4)
            {
                return Enumerable.Repeat(1, light + shade - 1);
            }
            if (light > 0 && shade > 0)
            {
                return Enumerable.Concat(
                    Enumerable.Repeat(1, light - 1),
                    Enumerable.Concat(
                        Enumerable.Repeat(2, 1),
                        Enumerable.Repeat(1, shade - 1)
                        )
                    );
            } else if (light < 5 && shade < 5)
            {
                return Enumerable.Repeat(1, light + shade - 1);
            } else
            {
                Debug.Assert(light == 6 || shade == 6);
                return new int[] { 1, 2, 1, 1, 1 };
            }
        }

        private IEnumerable<TimeSpan> Beats(byte light, byte shade)
        {
            var reading = Reading;
            var first = reading + interval + interval - new TimeSpan(reading.Ticks % interval.Ticks);
            if (light == 0 && shade == 0) yield break;
            yield return first;
            foreach (int pause in Pauses(light, shade))
            {
                first += new TimeSpan(interval.Ticks * pause);
                yield return first;
            }
        }

        private async Task RunnerTask(int row, byte light, byte shade, CancellationToken token)
        {
            TimeSpan[] beats = Beats(light, shade).ToArray();
            lock (hintBeats) hintBeats[row] = beats;
            int commonNum = light + shade;
            for (int i = 0; i < commonNum; ++i)
            {
                await Task.Delay(beats[i] - stopwatch.Elapsed, token);
                if (i < light) lightMatch.Play();
                else shadeMatch.Play();
            }
        }

        private Rectangle HintPosition(int row, int column)
        {
            return new Rectangle(new Point(drawableArea.Left + column * hintArea.X, drawableArea.Bottom - (row + 1) * hintArea.Y), hintArea);
        }

        private TimeSpan Reading { get => stopwatch.Elapsed + negshift; }
    }
}
