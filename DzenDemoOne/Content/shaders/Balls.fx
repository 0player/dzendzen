﻿#if OPENGL
#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0
#else
#define VS_SHADERMODEL vs_4_0
#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture : register(t0);
Texture2D SceneTexture;

sampler2D SpriteTextureSampler : register(s0);

sampler2D SceneTextureSampler = sampler_state
{
	Texture = <SceneTexture>;
	AddressU = Border;
	AddressV = Border;
	BorderColor = 0x00000000;
};

Texture2D reflections;
Texture2D refractions;
sampler2D reflectionsSampler = sampler_state {
	Texture = <reflections>; 
	AddressU = Clamp;
	AddressV = Clamp;
};
sampler2D refractionsSampler = sampler_state { 
	Texture = <refractions>;
	AddressU = Clamp;
	AddressV = Clamp;
};

cbuffer lightingModel {
	float4 skyColor;
	float transparency;
};

cbuffer worldData
{
	float4x4 matWorld;
};

cbuffer sceneData
{
	int rows;
	int columns;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
	float2 BallSceneCoordinates : TEXCOORD1;
};

VertexShaderOutput MainVS(float4 position: POSITION0, float4 color : COLOR0, float2 texCoord : TEXCOORD0)
{
	VertexShaderOutput output;
	output.Position = mul(position, matWorld);
	output.Color = color.aaaa;
	output.TextureCoordinates = texCoord;
	output.BallSceneCoordinates.x = (color.g * 255 + 0.5) / float(columns);
	output.BallSceneCoordinates.y = (color.r * 255 + 0.5) / float(rows);
	return output;
}

float4 mixThrough(float4 color, float4 ballColor) {
	if (!any(ballColor))
		return color;
	return color * ballColor + color * (1 - ballColor) * transparency;
}

float4 refractSide(float4 ballColor, float4 nbhColor) {
	if (!any(nbhColor))
		return 0;
	float4 afterNbh = mixThrough(skyColor, nbhColor);
	float4 afterSide = mixThrough(afterNbh, ballColor);
	return mixThrough(nbhColor, ballColor);
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float4 spriteColor = tex2D(SpriteTextureSampler, input.TextureCoordinates);
	float2 centered = (input.TextureCoordinates - float2(0.5, 0.5)) * 2;
	float radius = distance(centered, float2(0, 0));
	float ballIsGood = radius > 1.0 ? 0.0 : 1.0f;

	float2 diagOffset = float2(1.0 / float(columns), -1.0 / float(rows));
	if (centered.x < 0) diagOffset.x *= -1;
	if (centered.y < 0) diagOffset.y *= -1;

	float2 sideOffset = diagOffset;
	float2 mapIndex = abs(centered);
	if (mapIndex.x > mapIndex.y) {
		sideOffset.y = 0;
	}
	else {
		mapIndex = mapIndex.yx;
		sideOffset.x = 0;
	}

	float4 refraction = tex2D(refractionsSampler, mapIndex);
	float4 reflection = tex2D(reflectionsSampler, mapIndex);


	float4 ballScene = tex2D(SceneTextureSampler, input.BallSceneCoordinates);

	float4 refractionSideScene = tex2D(SceneTextureSampler, input.BallSceneCoordinates - sideOffset);
	float4 refractionDiagScene = tex2D(SceneTextureSampler, input.BallSceneCoordinates - diagOffset);
	float skyRefr = (any(refractionSideScene) && refraction.r || any(refractionDiagScene) && refraction.g) ? refraction.b : refraction.a;
	float4 refractionColor = 0;// mixThrough(skyColor, ballScene) * (refraction.a - refraction.r - refraction.g);
	refractionColor += refractSide(ballScene, refractionSideScene) * refraction.r;
	refractionColor += refractSide(ballScene, refractionDiagScene) * refraction.g;

	float4 reflectionSideScene = tex2D(SceneTextureSampler, input.BallSceneCoordinates + sideOffset);
	float4 reflectionDiagScene = tex2D(SceneTextureSampler, input.BallSceneCoordinates + diagOffset);
	float skyRefl = (any(reflectionSideScene) && reflection.r || any(reflectionDiagScene) && reflection.g) ? reflection.b : reflection.a;
	float4 reflectionColor = ballScene * (reflection.a - reflection.r - reflection.g);
	reflectionColor += mixThrough(skyColor, reflectionSideScene) * ballScene * reflection.r;
	reflectionColor += mixThrough(skyColor, reflectionDiagScene) * ballScene * reflection.g;

	return (refractionColor + reflectionColor + spriteColor * 0.000001) * input.Color;
}

technique SpriteDrawing
{
	pass P0
	{
		VertexShader = compile VS_SHADERMODEL MainVS();
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};