﻿using DzenDemoOne;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DzenDzen
{
    public class BallPitView : DrawableGameComponent
    {
        private abstract class CellAnimation : IAnimation
        {
            public BallType Ball { get; private set; } = BallType.NIHIL;
            private CellAnimation next;
            private CellAnimation wait;
            private bool waiting = false;
            private readonly BallPitView view;
            private AnimationProgress progress;
            public int? TargetRow { get; private set; }
            public int? TargetColumn { get; private set; }
            private readonly string sound;

            public CellAnimation(BallPitView view, AnimationProgress progress, int? targetRow = null, int? targetColumn = null, string sound = null)
            {
                this.view = view;
                this.progress = progress;
                TargetRow = targetRow;
                TargetColumn = targetColumn;
                this.sound = sound;

                progress.Add(this);
            }

            public bool IsStarted => progress.IsStarted;
            public bool IsFinished => progress.IsFinished;


            public void SetNext(CellAnimation anim)
            {
                Debug.Assert(next == null);
                next = anim;
                anim.Ball = Ball;
                progress.SpeedUp();
            }

            public void SetWaiting(CellAnimation anim)
            {
                Debug.Assert(wait == null);
                if (anim.progress == progress) return;
                wait = anim;
                anim.waiting = true;
            }

            public void Enqueue(BallType ball)
            {
                Ball = ball;
                GoIfNotWaiting();
            }

            private void Go()
            {
                Debug.Assert(waiting);
                waiting = false;
                GoIfNotWaiting();
            }

            private void GoIfNotWaiting()
            {
                if (waiting) return;
                Debug.Assert(Ball != BallType.NIHIL);
                view.AddActive(this);
                view.PlaySound(sound);
                progress.SetReady();
            }

            public abstract void OnStart();
            public abstract void OnAnimate(float progress);
            public abstract void Draw(SpriteBatch spriteBatch, Texture2D sprite);

            public virtual void OnFinish()
            {
                if (next != null)
                {
                    next.Enqueue(Ball);
                }
                else if (TargetRow != null && TargetColumn != null)
                {
                    view.BallArrived(this);
                }
                wait?.Go();
            }
        }

        private class BallMoveAnimation : CellAnimation, IAnimation
        {
            private Rectangle sourceRect;
            private Rectangle targetRect;
            private readonly Color sceneCoord;
            public Rectangle rect;

            public BallMoveAnimation(BallPitView view, AnimationProgress progress, Rectangle sourceRect, Rectangle targetRect, Color sceneCoord, int? targetRow = null, int? targetColumn = null, string sound = null)
                : base(view, progress, targetRow, targetColumn, sound)
            {
                this.sourceRect = sourceRect;
                this.targetRect = targetRect;
                this.sceneCoord = sceneCoord;
            }

            public override void OnStart()
            {
                rect = sourceRect;
            }

            public override void OnAnimate(float p)
            {
                var newRect = new Rectangle(
                    (int)(sourceRect.X + (targetRect.X - sourceRect.X) * p),
                    (int)(sourceRect.Y + (targetRect.Y - sourceRect.Y) * p),
                    (int)(sourceRect.Width + (targetRect.Width - sourceRect.Width) * p),
                    (int)(sourceRect.Height + (targetRect.Height - sourceRect.Height) * p)
                    );
                rect = newRect;
            }

            public override void OnFinish()
            {
                rect = targetRect;
                base.OnFinish();
            }

            public override void Draw(SpriteBatch spriteBatch, Texture2D sprite)
            {
                spriteBatch.Draw(sprite, rect, sceneCoord);
            }
        }

        private class BallFadeAnimation : CellAnimation
        {
            private readonly Rectangle targetRect;
            private Color sceneColor;
            private readonly float fadeStart;
            private readonly float fadeLength;


            public BallFadeAnimation(BallPitView view, AnimationProgress progress, Rectangle targetRect, Color sceneColor, float fadeStart, float fadeLength) : base(view, progress)
            {
                progress.TweeningFunction = AnimationProgress.LinearTween;
                this.targetRect = targetRect;
                this.fadeStart = fadeStart;
                this.fadeLength = fadeLength;
                this.sceneColor = sceneColor;
            }

            public override void Draw(SpriteBatch spriteBatch, Texture2D sprite)
            {
                spriteBatch.Draw(sprite, targetRect, sceneColor);
            }

            public override void OnAnimate(float progress)
            {
                progress = Math.Max(0f, Math.Min(1f, (progress - fadeStart) / fadeLength));
                sceneColor.A = (byte)(255 * (1.0f - progress));
            }

            public override void OnStart()
            {
            }
        }

        private struct CellData
        {
            private CellAnimation incoming;
            private CellAnimation outgoing;
            private BallType ballHere;

            public void Fill(BallType ball)
            {
                Debug.Assert(ball != BallType.NIHIL);
                Debug.Assert(ballHere == BallType.NIHIL);
                Debug.Assert(incoming != null);
                Debug.Assert(outgoing == null || outgoing.IsFinished);
                ballHere = ball;
                incoming = null;
                outgoing = null;
            }

            public void Drain(CellAnimation anim)
            {
                Debug.Assert((incoming != null) ^ (ballHere != BallType.NIHIL));
                if (ballHere != BallType.NIHIL)
                {
                    anim.Enqueue(ballHere);
                    ballHere = BallType.NIHIL;
                    outgoing = anim;
                }
                else
                {
                    incoming.SetNext(anim);
                    incoming = null;
                    outgoing = anim;
                }
            }

            public void SetIncoming(CellAnimation anim)
            {
                Debug.Assert(incoming == null && ballHere == BallType.NIHIL);
                incoming = anim;
                if (outgoing?.IsFinished ?? true)
                {
                    outgoing = null;
                }
                else
                {
                    outgoing.SetWaiting(anim);
                }
            }

            public bool Empty => incoming == null && ballHere == BallType.NIHIL;

            public BallType Ball => ballHere;

            public BallType SceneBall
            {
                get
                {
                    if (ballHere != BallType.NIHIL) return ballHere;
                    if (outgoing != null && !outgoing.IsFinished && outgoing.TargetRow == null) return outgoing.Ball;
                    if (incoming != null && incoming.IsStarted && incoming.Ball != BallType.NIHIL) return incoming.Ball;
                    if (outgoing != null && !outgoing.IsFinished) return outgoing.Ball;
                    return BallType.NIHIL;
                }
            }
        }

        private readonly CellData[,] staticCells;
        private List<AnimationProgress> progresses;
        private List<CellAnimation> activeAnims;

        private Rectangle BallAt(int row, int column)
        {
            return new Rectangle(
                drawableArea.Left + column * ballSize,
                drawableArea.Bottom - (row + 1) * ballSize,
                ballSize,
                ballSize
                );
        }

        private Color SceneCoord(int row, int column)
        {
            return new Color(row, column, 0, 255);
        }

        private ref CellData AnimCell(CellAnimation anim)
        {
            return ref staticCells[anim.TargetRow.Value, anim.TargetColumn.Value];
        }

        private void BallArrived(CellAnimation anim)
        {
            AnimCell(anim).Fill(anim.Ball);
        }

        private void PlaySound(string sound)
        {
            if (sound != null)
                jukebox.PlaySound(sound);
        }

        private void AddIncoming(CellAnimation anim, BallType ball)
        {
            AnimCell(anim).SetIncoming(anim);
            anim.Enqueue(ball);
        }

        private void SetIncoming(CellAnimation anim)
        {
            AnimCell(anim).SetIncoming(anim);
        }

        private void AddOutgoing(CellAnimation anim, int sourceRow, int sourceColumn)
        {
            staticCells[sourceRow, sourceColumn].Drain(anim);
        }

        private void AddActive(CellAnimation anim)
        {
            activeAnims.Add(anim);
        }

        private AnimationProgress NewAnimationProgress(TimeSpan lowSpeedDuration)
        {
            var res = new AnimationProgress(lowSpeedDuration);
            progresses.Add(res);
            return res;
        }

        private AnimationProgress NewAnimationProgress(TimeSpan lowSpeedDuration, TimeSpan highSpeedDuration)
        {
            var res = new AnimationProgress(lowSpeedDuration, highSpeedDuration);
            progresses.Add(res);
            return res;
        }

        public void ScrollToLeft(int row, int column, BallType newType)
        {
            Rectangle cellRect = BallAt(row, column);
            Rectangle leftBorder = new Rectangle(cellRect.Left, cellRect.Top, 0, cellRect.Height);
            Rectangle rightBorder = new Rectangle(cellRect.Right, cellRect.Top, 0, cellRect.Height);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            AddOutgoing(new BallMoveAnimation(this, progress, cellRect, leftBorder, SceneCoord(row, column)), row, column);
            AddIncoming(new BallMoveAnimation(this, progress, rightBorder, cellRect, SceneCoord(row, column), row, column), newType);
            progress.Start();
        }

        public void ScrollToRight(int row, int column, BallType newType)
        {
            Rectangle cellRect = BallAt(row, column);
            Rectangle leftBorder = new Rectangle(cellRect.Left, cellRect.Top, 0, cellRect.Height);
            Rectangle rightBorder = new Rectangle(cellRect.Right, cellRect.Top, 0, cellRect.Height);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            AddOutgoing(new BallMoveAnimation(this, progress, cellRect, rightBorder, SceneCoord(row, column)), row, column);
            AddIncoming(new BallMoveAnimation(this, progress, leftBorder, cellRect, SceneCoord(row, column), row, column), newType);
            progress.Start();
        }

        public void SlideToLeft(int row, int column)
        {
            Debug.Assert(column >= 1);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            var anim = new BallMoveAnimation(this, progress, BallAt(row, column), BallAt(row, column - 1), SceneCoord(row, column-1), row, column - 1);
            AddOutgoing(anim, row, column);
            SetIncoming(anim);
            progress.Start();
        }

        public void SlideToRight(int row, int column)
        {
            Debug.Assert(column <= numCols - 2);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            var anim = new BallMoveAnimation(this, progress, BallAt(row, column), BallAt(row, column + 1), SceneCoord(row, column+1), row, column + 1);
            AddOutgoing(anim, row, column);
            SetIncoming(anim);
            progress.Start();
        }

        public void SlideDown(int row, int column, BallType newType = BallType.NIHIL)
        {
            Debug.Assert(row >= 1);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            var sourcePos = BallAt(row, column);
            var anim = new BallMoveAnimation(this, progress, sourcePos, BallAt(row - 1, column), SceneCoord(row-1, column), row - 1, column, "Drop");
            AddOutgoing(anim, row, column);
            SetIncoming(anim);
            if (newType != BallType.NIHIL)
            {
                var topBorder = new Rectangle(sourcePos.Left, sourcePos.Top, sourcePos.Width, 0);
                AddIncoming(new BallMoveAnimation(this, progress, topBorder, sourcePos, SceneCoord(row, column), row, column), newType);
            }
            progress.Start();
        }

        public void SlideUpInto(int row, int column)
        {
            Debug.Assert(row >= 1);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            var sourcePos = BallAt(row, column);
            var topBorder = new Rectangle(sourcePos.Left, sourcePos.Top, sourcePos.Width, 0);
            AddOutgoing(new BallMoveAnimation(this, progress, sourcePos, topBorder, SceneCoord(row, column)), row, column);
            var anim = new BallMoveAnimation(this, progress, BallAt(row - 1, column), sourcePos, SceneCoord(row-1, column), row, column, "Pull");
            AddOutgoing(anim, row - 1, column);
            SetIncoming(anim);
            progress.Start();
        }

        public void SwapWithLower(int row, int column)
        {
            Debug.Assert(row >= 1);
            var progress = NewAnimationProgress(new TimeSpan(0, 0, 0, 0, 100));
            var sourcePos = BallAt(row, column);
            var targetPos = BallAt(row - 1, column);
            // order is important
            var revAnim = new BallMoveAnimation(this, progress, targetPos, sourcePos, SceneCoord(row, column), row, column);
            var fwdAnim = new BallMoveAnimation(this, progress, sourcePos, targetPos, SceneCoord(row-1, column), row - 1, column, "Drop");
            AddOutgoing(revAnim, row - 1, column);
            AddOutgoing(fwdAnim, row, column);
            SetIncoming(revAnim);
            SetIncoming(fwdAnim);
            progress.Start();
        }

        public void SetBall(int row, int column, BallType ball)
        {
            Debug.Assert(ball != BallType.NIHIL);
            var progress = NewAnimationProgress(new TimeSpan(0));
            var sourcePos = BallAt(row, column);
            var nullPos = new Rectangle(sourcePos.Center, new Point(0, 0));
            AddIncoming(new BallMoveAnimation(this, progress, nullPos, sourcePos, SceneCoord(row, column), row, column), ball);
        }

        private void ScreenFade(TimeSpan fadeTime, int maxRows)
        {
            // It sure will look like rows fading one after another, but in reality they exist in preordained harmony.
            var leibnitzProgress = NewAnimationProgress(fadeTime);

            float filledRows = 0;
            float onePerRow = 1.0f / maxRows;
            for (int row = maxRows - 1; row >= 0; --row)
            {
                for (int column = 0; column < numCols; ++column)
                {
                    if (!staticCells[row, column].Empty)
                    {
                        AddOutgoing(new BallFadeAnimation(this, leibnitzProgress, BallAt(row, column), SceneCoord(row, column), filledRows / maxRows, onePerRow), row, column);
                    }
                }
                filledRows += 1.0f;
            }
            leibnitzProgress.Start();
        }

        public void ScreenFadeWin(int maxRows)
        {
            ScreenFade(clearScreenWin.Duration, maxRows);
            clearScreenWin.Play();
        }

        public void ScreenFadeLose(int maxRows)
        {
            ScreenFade(clearScreenLose.Duration, maxRows);
            clearScreenLose.Play();
        }

        public bool IsShade { get; set; } = false;

        private Texture2D BallSprite(BallType ball)
        {
            Debug.Assert(ball != BallType.NIHIL);
            return ballSprites[IsShade ? ball.Shade() : ball];
        }

        private readonly int ballSize;
        private readonly int numRows;
        private readonly int numCols;
        private Rectangle drawableArea;
        private SpriteBatch spriteBatch;
        private Dictionary<BallType, Texture2D> ballSprites;
        private SoundEffect clearScreenWin;
        private SoundEffect clearScreenLose;
        private Jukebox jukebox;
        private Texture2D scene;
        private Effect effect;
        private Texture2D refractionMap;
        private Texture2D reflectionMap;

        private readonly Dictionary<BallType, Color> ballColors = new Dictionary<BallType, Color>{
            {BallType.NIHIL, Color.TransparentBlack },
            {BallType.Aqua, Color.Blue },
            {BallType.Caeli, Color.White },
            {BallType.Glacies, Color.Cyan },
            {BallType.Herba, Color.LawnGreen },
            {BallType.Ignis, Color.OrangeRed },
            {BallType.Terra, Color.Gray }
        };

        public BallPitView(Game game, Jukebox jukebox, int rows, int columns, int ball_size, Rectangle drawableArea) : base(game)
        {
            numRows = rows;
            numCols = columns;
            ballSize = ball_size;

            this.jukebox = jukebox;
            this.drawableArea = drawableArea;

            staticCells = new CellData[rows, columns];
            progresses = new List<AnimationProgress>();
            activeAnims = new List<CellAnimation>();
        }

        protected override void LoadContent()
        {
            var Content = Game.Content;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            scene = new Texture2D(GraphicsDevice, numCols, numRows);

            ballSprites = new Dictionary<BallType, Texture2D>
            {
                [BallType.Herba] = Content.Load<Texture2D>("balls/Herba"),
                [BallType.Ignis] = Content.Load<Texture2D>("balls/Ignis"),
                [BallType.Aqua] = Content.Load<Texture2D>("balls/Aqua"),
                [BallType.Terra] = Content.Load<Texture2D>("balls/Terra"),
                [BallType.Caeli] = Content.Load<Texture2D>("balls/Caeli"),
                [BallType.Glacies] = Content.Load<Texture2D>("balls/Glacies")
            };

            clearScreenWin = Content.Load<SoundEffect>("music/CLRSCRWin");
            clearScreenLose = Content.Load<SoundEffect>("music/CLRSCRLose");

            refractionMap = Content.Load<Texture2D>("shaders/refractions");
            reflectionMap = Content.Load<Texture2D>("shaders/reflections");
            effect = Content.Load<Effect>("shaders/Balls");
            var vp = GraphicsDevice.Viewport;
            Matrix projection = Matrix.CreateOrthographicOffCenter(0, vp.Width, vp.Height, 0, 0, -1);
            effect.Parameters["matWorld"].SetValue(projection);
            effect.Parameters["SceneTexture"].SetValue(scene);
            effect.Parameters["rows"].SetValue(numRows);
            effect.Parameters["columns"].SetValue(numCols);
            effect.Parameters["refractions"].SetValue(refractionMap);
            effect.Parameters["reflections"].SetValue(reflectionMap);
            effect.Parameters["skyColor"].SetValue(Color.White.ToVector4());
            //effect.Parameters["transparency"].SetValue(0.30430593406137685f);
            effect.Parameters["transparency"].SetValue(0.1f);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            progresses.RemoveAll(x => x.IsFinished);

            if (gameTime.IsRunningSlowly)
            {
                progresses.ForEach(x => x.Animate(gameTime));
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            progresses.ForEach(x => x.Animate(gameTime));
            activeAnims.RemoveAll(x => x.IsFinished);

            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, effect);

            var sceneColors = new Color[numRows * numCols];

            for (int row = 0; row < numRows; ++row)
            {
                for (int column = 0; column < numCols; ++column)
                {
                    sceneColors[row * numCols + column] = ballColors[staticCells[row, column].SceneBall];
                }
            }

            scene.SetData(sceneColors);

            for (int row = 0; row < numRows; ++row)
            {
                for (int column = 0; column < numCols; ++column)
                {
                    var ball = staticCells[row, column].Ball;
                    if (ball != BallType.NIHIL)
                    {
                        spriteBatch.Draw(BallSprite(ball), BallAt(row, column), SceneCoord(row, column));
                    }
                }
            }

            foreach (var anim in activeAnims)
            {
                var ball = anim.Ball;
                Debug.Assert(ball != BallType.NIHIL);
                anim.Draw(spriteBatch, BallSprite(ball));
                Debug.Assert(anim.TargetRow == null || AnimCell(anim).Ball == BallType.NIHIL);
            }

            var sceneRect = new Rectangle(0, 0, scene.Width * 4, scene.Height * 4);
            //spriteBatch.Draw(scene, sceneRect, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}