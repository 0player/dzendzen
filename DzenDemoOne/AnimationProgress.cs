﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DzenDzen
{
    public interface IAnimation
    {
        void OnStart();
        void OnFinish();
        void OnAnimate(float progress);
    }

    public class AnimationProgress
    {
        int startSemaphore = 0;
        bool canGo = false;
        bool isSpeedy = false;

        public float Progress { get; private set; } = 0;

        long lowSpeedDurationTicks;
        long highSpeedDurationTicks;
        const long debugSlowdownFactor =
#if DEBUG
        10;
#else
        1;
#endif

        public Func<float, float> TweeningFunction = LinearTween;

        public AnimationProgress(TimeSpan duration)
        {
            lowSpeedDurationTicks = duration.Ticks * debugSlowdownFactor;
            highSpeedDurationTicks = lowSpeedDurationTicks / 4;
        }

        public AnimationProgress(TimeSpan duration, TimeSpan spedUpDuration)
        {
            lowSpeedDurationTicks = duration.Ticks * debugSlowdownFactor;
            highSpeedDurationTicks = spedUpDuration.Ticks * debugSlowdownFactor;
        }

        List<IAnimation> animations = new List<IAnimation>();

        private void Finish()
        {
            animations.ForEach(x => x.OnFinish());
            animations = null;
        }

        public void Add(IAnimation anim)
        {
            Debug.Assert(!canGo);
            animations.Add(anim);
            ++startSemaphore;
        }

        public void SetReady()
        {
            --startSemaphore;
            Debug.Assert(startSemaphore >= 0);
            if (IsStarted)
                animations.ForEach(x => x.OnStart());
        }

        public void Start()
        {
            if (!canGo)
            {
                canGo = true;
                if (IsStarted)
                    animations.ForEach(x => x.OnStart());
            }
        }

        public void Animate(GameTime gameTime)
        {
            if (IsStarted && !IsFinished)
            {
                long ticks = isSpeedy ? highSpeedDurationTicks : lowSpeedDurationTicks;
                float ratio = ticks > 0 ? (float)gameTime.ElapsedGameTime.Ticks / ticks : 1.0f;
                Progress = Math.Min(1.0f, Progress + ratio);
                animations.ForEach(x => x.OnAnimate(Progress));
                if (IsFinished)
                    Finish();
            }
            else if (!IsStarted)
            {
                Start();
            }
        }

        public void ForceFinish()
        {
            Debug.Assert(startSemaphore == 0);
            if (!IsStarted)
            {
                Start();
            }
            if (!IsFinished)
            {
                animations.ForEach(x => x.OnAnimate(1.0f));
                Finish();
            }
        }

        public bool IsStarted
        {
            get
            {
                return canGo && startSemaphore == 0;
            }
        }
        public bool IsFinished { get { return Progress == 1.0; } }

        public void SpeedUp() { isSpeedy = true; }

        public static float LinearTween(float x) => x;
        public static float CubicTween(float x) => x * x * (3 - x - x);
    }
}
