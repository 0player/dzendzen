# -*- coding: utf-8 -*-
"""
Created on Mon Jan  7 13:18:32 2019

@author: Алекс
"""

import math
import numpy
import random

R = 17

D = 36

N = 2.5

centers = [
        (0, 0, -D),
        (1, -D, -D)
        ]

def sign(arg):
    return (arg > 0) - (arg < 0)

def indices_for_gammasq(gammasq):
    cosa = math.sqrt(1 - gammasq)
    cosb = math.sqrt(1 - gammasq / N / N)
    Rs = ((cosa - N * cosb)/(cosa + N * cosb)) ** 2
    Rp = ((cosb - N * cosa)/(cosb + N * cosa)) ** 2
    passthrough = ((1 - Rs)**2 + (1 - Rp)**2)/2
    reflect = (Rs + Rp) / 2
    transparency = math.exp(-math.sqrt(N * N - gammasq) * 0.5)
    
    return passthrough, reflect, transparency

def ray_hit_ball(cx, cy, sinp, cosp,
                 p0x, p0y, dx, dy):
    dtoplane = cosp * cx - sinp * cy
    cinplane = sinp * cx + cosp * cy
        
    t = -(p0x - cinplane) * dx - p0y * dy
    if t < 0:
        return False, 0.0
    dinplane2 = (p0x - cinplane) ** 2 + p0y ** 2 - t ** 2
    gammasq = (dtoplane * dtoplane + dinplane2) / R / R
    return gammasq < 1, gammasq
    

def ball_refraction_and_reflection(x, y):
    res = numpy.array([0, 0, # sky-refl, hit-sky-refl
                       0, 0, 0, 0, # ball, sky-refr, hit-ball, hit-sky-refr
                       0, 0, 0, 0, # for first refracted neighbour
                       0, 0, 0, 0, # for second refracted neighbour
                       0, 0, # for first reflected neighbour
                       0, 0, # for second reflected neighbour
                       ], dtype=float)
    
    if x * x + y * y > R * R:
        return res
    
    gammasq = (x * x + y * y) / (R * R)
    
    
    gamma = math.sqrt(gammasq)
    
    sina = gamma
    sinb = gamma / N
    cosa = math.sqrt(1 - gammasq)
    cosb = math.sqrt(1 - gammasq / N / N)
    
    sin2b = 2 * sinb * cosb
    cos2b = 1 - 2 * sinb * sinb
    
    sin2a = 2 * sina * cosa
    cos2a = 1 - 2 * sina * sina
    
    passthrough, reflect, transparency = indices_for_gammasq(gammasq)
    
    res[0] = reflect
    res[2] = passthrough
    res[3] = transparency * passthrough
    
    p0x = cosa * sin2b - sina * cos2b
    p0y = -cosa * cos2b - sina * sin2b
    p0x *= R
    p0y *= R
    
    dx = -sin2a * cos2b + cos2a * sin2b
    dy = -cos2a * cos2b - sin2a * sin2b
    
    d0 = math.hypot(x, y)
    
    try:
        sinp = x / d0
        cosp = y / d0
    except ZeroDivisionError:
        return res
    
    hit_sky = 1.0
    
    for i, cx, cy in centers:
        hit, nbh_sq = ray_hit_ball(cx, cy, sinp, cosp, p0x, p0y, dx, dy)
    
        if hit:    
            nbh_passthrough, nbh_reflect, nbh_transparency = indices_for_gammasq(nbh_sq)
            total_pt = passthrough * nbh_passthrough
            res[6+i*4:6+i*4+4] = [
                    total_pt,
                    total_pt * transparency,
                    total_pt * nbh_transparency,
                    total_pt * transparency * nbh_transparency
            ]
            hit_sky = nbh_reflect
            break
    
    res[4:6] = res[2:4] * hit_sky
    
    
    # now for reflections
    
    p0x = sina * R
    p0y = cosa * R
    dx = sin2a
    dy = cos2a
    
    sky_reflect = 1
    for i, cx, cy in centers:
        hit, nbh_sq = ray_hit_ball(-cx, -cy, sinp, cosp, p0x, p0y, dx, dy)
        
        if hit:
            nbh_passthrough, nbh_reflect, nbh_transparency = indices_for_gammasq(nbh_sq)
            res[14+i*2:14+i*2+2] = [
                    reflect * nbh_passthrough,
                    reflect * nbh_passthrough * nbh_transparency
                    ]
            sky_reflect = nbh_reflect
    
    res[1] = res[0] * sky_reflect
    
    return res


def avg_ratio(num, den, power):
    nonz = (num != 0) + (den != 0)
    r = (num[nonz]/den[nonz]) ** (1/power)
    avg = r.sum() / len(r)
    restored_error = numpy.abs(den[nonz] * avg ** power - num[nonz]).max()
    if restored_error > 5/255.:
        raise ValueError(restored_error)
    return r.sum(), len(r)


def montecarlo():
    maxsize = R
    counts = numpy.zeros((maxsize, maxsize), dtype=int)
    values = numpy.zeros((maxsize, maxsize, 18), dtype=float)
    
    for _ in range(100000):
        x = random.uniform(0, R)
        y = random.uniform(0, R)
        ix = min(R - 1, math.floor(x))
        iy = min(R - 1, math.floor(y))
        counts[ix, iy] += 1
        counts[iy, ix] += 1
        values[ix, iy, :] += ball_refraction_and_reflection(x, y)
        values[iy, ix, :] += ball_refraction_and_reflection(y, x)
    
    values /= counts.reshape((maxsize, maxsize, 1))
    return values

from PIL import Image

def make_image(arr):
    numpy.clip(arr, 0, 1, out=arr)
    out = numpy.zeros(arr.shape, dtype = numpy.uint8)
    numpy.rint(arr * 255, out=out, casting='unsafe')
    return Image.fromarray(out)

if __name__ == '__main__':
    res = montecarlo()
    blue = numpy.zeros(res.shape[:2])
    
    # calculate constant passthrough
    pt_total = 0.0
    pt_count = 0
    
    for i1, i2, power in [
            (2, 3, 1),
            (4, 5, 1),
            (6, 7, 1),
            (6, 8, 1),
            (6, 9, 2),
            (10, 11, 1),
            (10, 12, 1),
            (10, 13, 2),
            (14, 15, 1),
            (16, 17, 1)
            ]:
        try:
            pt_subtotal, pt_subcount = avg_ratio(res[:,:,i2], res[:,:,i1], power)
        except:
            print("failed at", i1, i2)
            raise
        pt_total += pt_subtotal
        pt_count += pt_subcount
    
    print("The average transparency is {}".format(pt_total / pt_count))
    
    make_image(res[:, :, [14, 16, 1, 0]]).save("reflections.png")
    make_image(res[:, :, [6, 10, 4, 2]]).save("refractions.png")