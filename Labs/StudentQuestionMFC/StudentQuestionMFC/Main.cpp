#include "afxwin.h" // MFC �������� � ����������� ����������
#include <ctime>
#include <time.h>
#include <math.h>
#define IDC_YESBUTTON 100	// ������������� ������
#define IDC_NOBUTTON 101	// ������������� ������
#define IDC_MYEDIT 102	// ������������� ���� ��������������
#define IDC_LISTLENAREA 103	// ������������� ���� �� �������
#define ID_TIMER_1 104
using namespace std;

class YesButton : public CButton
{
public:
	afx_msg void OnLButtonDown(UINT, CPoint);
private:
	DECLARE_MESSAGE_MAP(); // ������� �������� ������
};
void YesButton::OnLButtonDown(UINT, CPoint)
{
	MessageBox(_T("�� - ������� �������!"), _T("�������"), MB_ICONINFORMATION | MB_OK);
	AfxGetMainWnd()->SendMessage(WM_CLOSE, 0, 0);
}
BEGIN_MESSAGE_MAP(YesButton, CButton) // ������� �������� �� ���������
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()
class NoButton : public CButton
{
public:
	afx_msg void OnLButtonDown(UINT, CPoint);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
private:
	DECLARE_MESSAGE_MAP(); // ������� �������� ������
};

void NoButton::OnLButtonDown(UINT, CPoint)
{
	MessageBox(_T("�������� ������� �������!"), _T("��..."), MB_ICONINFORMATION | MB_OK);
	AfxGetMainWnd()->SendMessage(WM_CLOSE, 0, 0);
}
BEGIN_MESSAGE_MAP(NoButton, CButton) // ������� �������� �� ���������
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

class CMainWnd : public CFrameWnd
{
public:
	CMainWnd();
	void MoveNoBtn(CPoint mouse);
	CString GetIntFromList();	// �������� ������
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSelChange();
	afx_msg void OnTimer(UINT);		// ������� ������� �� ������
	void MoveNoBtnStep();
	bool Timer;				// ��������� �������� �������
	~CMainWnd();    // ����������
	LONG area; //����� �������
	//����� ���������� ��� ������
	LONG new_X;
	LONG new_Y;
	//============================
	//�������� ������������ ������
	LONG speed_X;
	LONG speed_Y;
	//============================
private:
	CStatic* SpeedLable;      // ������� ���������� ��� ������ ������
	CStatic* LenAreaLable;      // ������� ���������� ��� ������ ������
	CStatic* QuestionLable;      // ������� ���������� ��� ������ ������
	YesButton* YesBtn;// ������� ���������� ������
	NoButton* NoBtn;// ������� ���������� ������
	CEdit* SpeedTB;  // ������� ��������������
	CListBox *LenAreaCB;	// ��������� �� ������� ����������
	int csListTextItem;	// ������������ ��������
	DECLARE_MESSAGE_MAP(); // ������� ��������
};

static bool Between(int bound1, int middle, int bound2) {
	return (bound1 <= middle && middle <= bound2)
		|| (bound1 >= middle && middle >= bound2);
}

static void Mirror(LONG bound, LONG& pos) {
	pos = bound + (bound - pos);
}

void CMainWnd::OnTimer(UINT uTime)
{
	MoveNoBtnStep();
}
void CMainWnd::MoveNoBtnStep()
{
	CRect rectNoBtn;
	CRect rectOwn;
	CRect new_pos(0, 0, 0, 0);
	NoBtn->GetWindowRect(rectNoBtn);
	this->ScreenToClient(rectNoBtn);
	this->GetClientRect(rectOwn);
	if (rectNoBtn.left == new_X && rectNoBtn.top == new_Y)
	{//���� �������� ����� ���������, �� ���������� ������ � ���������� ���������� ��� ���� �������
		Timer = false;
		KillTimer(ID_TIMER_1);
		return;
	}
	else {//���� ��� �� ������ ���������� ������
		new_pos.left = Between(rectNoBtn.left, new_X, rectNoBtn.left + speed_X) ?
			new_X : rectNoBtn.left + speed_X;
		new_pos.top = Between(rectNoBtn.top, new_Y, rectNoBtn.top + speed_Y) ?
			new_Y : rectNoBtn.top + speed_Y;

		// Test for mirror
		long w = rectNoBtn.Width(), h = rectNoBtn.Height();
		while (true) {
			if (rectOwn.left >= new_pos.left) {
				Mirror(rectOwn.left, new_pos.left);
				Mirror(rectOwn.left, new_X);
				speed_X *= -1;
			}
			else if (rectOwn.right <= new_pos.left + w) {
				Mirror(rectOwn.right - w, new_pos.left);
				Mirror(rectOwn.right - w, new_X);
				speed_X *= -1;
			}
			else {
				break;
			}
		}

		while (true) {
			if (rectOwn.top >= new_pos.top) {
				Mirror(rectOwn.top, new_pos.top);
				Mirror(rectOwn.top, new_Y);
				speed_Y *= -1;
			}
			else if (rectOwn.bottom <= new_pos.top + h) {
				Mirror(rectOwn.bottom - h, new_pos.top);
				Mirror(rectOwn.bottom - h, new_Y);
				speed_Y *= -1;
			}
			else {
				break;
			}
		}

		ASSERT(new_X <= new_pos.left && speed_X <= 0 || new_X >= new_pos.left && speed_X >= 0);
		ASSERT(new_Y <= new_pos.top && speed_Y <= 0 || new_Y >= new_pos.top && speed_Y >= 0);

		new_pos.right = new_pos.left + w;
		new_pos.bottom = new_pos.top + h;

		CRect check_rect;
		ASSERT(check_rect.IntersectRect(new_pos, rectOwn) && check_rect.EqualRect(new_pos));
		NoBtn->MoveWindow(new_pos, TRUE);
	}
}
void CMainWnd::OnSelChange()
{
	CString text;
	int index = LenAreaCB->GetCurSel();
	LenAreaCB->GetText(index, text);
	area = _wtol(text);
}
void NoButton::OnMouseMove(UINT nFlags, CPoint point){
	CPoint pos = point;//�������� ���������� ����
	((CMainWnd*)this->GetParent())->MoveNoBtn(pos);
}
void CMainWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rect;
	NoBtn->GetWindowRect(&rect);
	CPoint pos = point;//�������� ���������� ����
	if (pos.x >= rect.left - area
		&& pos.x <= rect.left + rect.Height() + area
		&& pos.y >= rect.top - area
		&& pos.y <= rect.top + rect.Width() + area)
		MoveNoBtn(pos);
}
BEGIN_MESSAGE_MAP(CMainWnd, CFrameWnd)	// ������� �������� �� ���������
	ON_WM_MOUSEMOVE()
	ON_LBN_SELCHANGE(IDC_LISTLENAREA, OnSelChange)
	ON_WM_TIMER()		// ����������� �� ������
END_MESSAGE_MAP()
CMainWnd::CMainWnd()
{
	Create(NULL, TEXT("Student Question MFC"), WS_OVERLAPPEDWINDOW, rectDefault,NULL, NULL);// ������� ���� ���������
	SpeedLable = new CStatic();
	SpeedLable->Create(TEXT("��������"), WS_CHILD | WS_VISIBLE | SS_CENTER,CRect(10, 10, 100, 50), this);	// �������
	SpeedTB = new CEdit();
	SpeedTB->Create(WS_CHILD | WS_VISIBLE | WS_BORDER, CRect(110, 10, 340, 50), this, IDC_MYEDIT);

	LenAreaLable = new CStatic();
	LenAreaLable->Create(TEXT("����"), WS_CHILD | WS_VISIBLE | SS_CENTER, CRect(350, 10, 450, 50), this);	// �������
	LenAreaCB = new CListBox();	// ������� ������ ������
	LenAreaCB->Create(WS_CHILD | WS_VISIBLE | WS_TABSTOP | LBS_NOTIFY | WS_VSCROLL | WS_BORDER, CRect(460, 10, 560, 100), this, IDC_LISTLENAREA);	// � ��� ������ ������ ��� � ���������������
	LenAreaCB->AddString(TEXT("25"));	// �������� ������
	LenAreaCB->AddString(TEXT("30"));	// ���
	LenAreaCB->AddString(TEXT("35"));
	LenAreaCB->AddString(TEXT("40"));   // ���	
	LenAreaCB->SetCurSel(0);
	Timer = false;
	CString text;
	int index = LenAreaCB->GetCurSel();
	LenAreaCB->GetText(index, text);
	area = _wtol(text);
	QuestionLable = new CStatic();
	QuestionLable->Create(TEXT("�������� �� �� ������ ���������?"), WS_CHILD | WS_VISIBLE | SS_CENTER, CRect(10, 70, 450, 120), this);	// �������
	
	YesBtn = new YesButton();  // ������ �����, �� ������ �������� ��������� ������
	YesBtn->Create(TEXT("��"), WS_CHILD | WS_VISIBLE | SS_CENTER, CRect(10, 130, 220, 180), this, IDC_YESBUTTON);
	NoBtn = new NoButton();  // ������ �����, �� ������ �������� ��������� ������
	NoBtn->Create(TEXT("���"), WS_CHILD | WS_VISIBLE | SS_CENTER, CRect(300, 130, 510, 180), this, IDC_NOBUTTON);
	
}
class CMyApp : public CWinApp
{
public:
	CMyApp();			//����������� �� ���������
	virtual BOOL InitInstance();//����������� �������������
};
CMyApp::CMyApp() // ����������� �������� ������ ����������
{}
BOOL CMyApp::InitInstance() // ����������� �������������
{
	m_pMainWnd = new CMainWnd();	// ������� ����� ����
	ASSERT(m_pMainWnd);	// ��������� ��� ������������
	m_pMainWnd->ShowWindow(SW_SHOW);// �������� ����
	m_pMainWnd->UpdateWindow();	// �������� ����
	return TRUE;		// ������� ��� ��� ���������
};

CMainWnd::~CMainWnd()
{
	delete SpeedLable;	// ������� ������������ ������
	delete SpeedTB;	// ������� ������������ ������
	delete LenAreaLable;	// ������� ������������ ������
	delete LenAreaCB;	// ������� ������������ ������
	delete QuestionLable;	// ������� ������������ ������
	delete YesBtn;	// ������� ������������ ������
	delete NoBtn;	// ������� ������������ ������
}
CMyApp theApp;	// ������ ����������

static long TwoIntervalRandom(long min1, long max1, long min2, long max2) {
	long total_size = (max1 - min1) + (max2 - min2);
	int max_allowed_rnd = RAND_MAX - (RAND_MAX % total_size + 1) % total_size;
	int rnd;
	do {
		rnd = rand();
	} while (rnd > max_allowed_rnd);
	rnd %= total_size;
	if (rnd < (max1 - min1)) {
		return min1 + rnd;
	}
	return (rnd - (max1 - min1)) + min2;
}

long sign(long x) {
	return (x > 0) - (x < 0);
}

long RDiv(long dividend, long divisor) {
	unsigned long udiv = (unsigned long)divisor;
	if (dividend < 0) {
		return -(long)(((unsigned long)-dividend + udiv / 2) / udiv);
	}
	return (long)(((unsigned long)dividend + udiv / 2) / udiv);
}

void CMainWnd::MoveNoBtn(CPoint mouse)
{
	//(rand() % 999) + 1;
	CRect rectNoBtn, rectOwn;
	NoBtn->GetWindowRect(rectNoBtn);
	this->ScreenToClient(rectNoBtn);
	// We only want to include client area here
	this->GetClientRect(rectOwn);
	long W = rectOwn.Width(), buttonW = rectNoBtn.Width();
	long H = rectOwn.Height(), buttonH = rectNoBtn.Height();

	// far enough from the mouse. These are all in client coordinates
	long minMinX = -2 * W;
	long minMaxX = mouse.x - buttonW - 5 * area;
	long maxMinX = mouse.x + 5 * area;
	long maxMaxX = 2 * W - buttonW;

	long minMinY = -2 * H;
	long minMaxY = mouse.y - buttonH - 5 * area;
	long maxMinY = mouse.y + 5 * area;
	long maxMaxY = 2 * H - buttonH;

	new_X = TwoIntervalRandom(minMinX, minMaxX, maxMinX, maxMaxX);
	new_Y = TwoIntervalRandom(minMaxY, minMaxY, maxMinY, maxMaxY);

	CString leAr;
	SpeedTB->GetWindowText(leAr);
	long sp = _wtol(leAr.GetBuffer());
	if (sp == 0)
		sp = 10;
	long delta_X = new_X - rectNoBtn.left;
	long delta_Y = new_Y - rectNoBtn.top;
	long delta = (abs(delta_X) + abs(delta_Y)) / 2;

	speed_X = RDiv(delta_X * sp * 10, delta);
	speed_Y = RDiv(delta_Y * sp * 10, delta);

	ASSERT(new_X <= rectNoBtn.left && speed_X <= 0 || new_X >= rectNoBtn.left && speed_X >= 0);
	ASSERT(new_Y <= rectNoBtn.top && speed_Y <= 0 || new_Y >= rectNoBtn.top && speed_Y >= 0);

	Timer = true;
	SetTimer(ID_TIMER_1, 50, nullptr);
	// Inch it away a couple of times
	MoveNoBtnStep();
	MoveNoBtnStep();
}