#include <afxwin.h>
#include <string.h>
#include <time.h>
#include <memory>

#include "resource.h"

class CMainWin : public CFrameWnd {
public:
	CMainWin(CString Title, int HowShow = SW_RESTORE);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT id);
private:
	// ������ �������� ������
	std::unique_ptr<CBitmap> backgroundBitmap;
	std::unique_ptr<CFont> imgFont;
	BOOL bflag = TRUE;
	FILE *f;
	CDC memDC;
	int image_number = 0;
	DECLARE_MESSAGE_MAP()
};

class CApp : public CWinApp {
public:
	BOOL InitInstance();
};

CApp App;

BEGIN_MESSAGE_MAP(CMainWin, CFrameWnd)
	ON_WM_TIMER()
	ON_WM_PAINT()
END_MESSAGE_MAP()

CMainWin::CMainWin(CString Title, int HowShow)
{
	f = fopen("config.ini", "r");
	RECT rect;
	rect.top = 0;
	rect.left = 0;
	// ����������, ���� ������ ���� ����� ������� ����, 
	// ���� ������� ���������� ������� ������� ���� 
	// ����� �������� ��������.
	rect.right = 800;//rect.left + bmp.bmWidth + (GetSystemMetrics(SM_CXDLGFRAME) + GetSystemMetrics(SM_CXEDGE)) * 2;

	rect.bottom = 600;//rect.top + bmp.bmHeight+ GetSystemMetrics(SM_CYCAPTION)+ (GetSystemMetrics(SM_CYDLGFRAME)+ GetSystemMetrics(SM_CYEDGE)) * 2; 

	this->Create(0, Title,
		WS_OVERLAPPED | WS_CAPTION | WS_DLGFRAME
		| WS_SYSMENU | WS_MINIMIZEBOX, rect, 0, 0);


	CClientDC cdc(this);
	memDC.CreateCompatibleDC(&cdc);

	imgFont = std::make_unique<CFont>();
	imgFont->CreateFont(1 * cdc.GetDeviceCaps(LOGPIXELSY), 0,
		0, 0, FW_NORMAL, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, PROOF_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, L"Arial");
	memDC.SelectObject(imgFont.get());
	memDC.SetBkMode(TRANSPARENT);

	// ���� �� ������ ������� ����, �� ���������� ���
	//if(x0 < 0 || y0 < 0) 
	this->CenterWindow();
	this->ShowWindow(HowShow);
	this->SetTimer(1, 3000, 0);
	this->OnTimer(1);
	this->UpdateWindow();
}

afx_msg void CMainWin::OnPaint()
{
	CPaintDC clientDC(this);
	// �������� �������������� �������� ������ � ��������� BITMAP
	BITMAP bmp;
	backgroundBitmap->GetBitmap(&bmp);
	// ����������� ������� ����� �� ��������� ������ � �������� 
	// ���������� ������� ����
	CRect rect;
	memDC.GetBoundsRect(rect, 0);
	clientDC.BitBlt(0, 0, bmp.bmWidth, bmp.bmHeight, &memDC, 0, 0, SRCCOPY);
}

afx_msg void CMainWin::OnTimer(UINT id) {
	if (fscanf(f, "%d", &bflag) < 1) {
		fseek(f, 0, SEEK_SET);
		fscanf(f, "%d", &bflag);
		image_number = 0;
	}
	++image_number;
	auto newBitmap = new CBitmap();
	if (bflag) { newBitmap->LoadBitmap(IDB_BITMAP1); }
	else { newBitmap->LoadBitmap(IDB_BITMAP2); }
	memDC.SelectObject(newBitmap);
	CString cs;
	cs.Format(L"Image #%d", image_number);
	memDC.TextOut(10, 10, cs);
	backgroundBitmap.reset(newBitmap);
	this->InvalidateRect(nullptr);
}

BOOL CApp::InitInstance()
{
	m_pMainWnd = new CMainWin("����� �������� ������",
		SW_RESTORE);
	//fclose(f);
	return TRUE;
}

